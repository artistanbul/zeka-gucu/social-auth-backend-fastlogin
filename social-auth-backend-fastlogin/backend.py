from requests.auth import HTTPBasicAuth
from social_core.backends.open_id_connect import OpenIdConnectAuth


class FastLogin(OpenIdConnectAuth):
    name = 'fastlogin'
    AUTHORIZATION_URL = 'https://hizligiris.com.tr/hizligiris/oauth/authorize'
    ACCESS_TOKEN_URL = 'https://hizligiris.com.tr/hizligiris/oauth/token'
    USERINFO_URL = 'https://hizligiris.com.tr/hizligiris/userinfo'
    DEFAULT_SCOPE = ['openid', 'phone', 'email', 'profile']
    USERNAME_KEY = 'sub'

    def auth_complete_credentials(self):
        return HTTPBasicAuth(*self.get_key_and_secret())

    def auth_params(self, state=None):
        params = super(FastLogin, self).auth_params(state)
        params.update({
            'acr_values': '2',
            'prompt': 'login',
            'display': 'page',
            'max_age': self.setting('MAX_AGE', '3600'),
            'ui_locales': self.setting('UI_LOCALES', 'en'),
            'claims_locales': self.setting('CLAIMS_LOCALES', 'en'),
        })
        return params

    def user_data(self, access_token, *args, **kwargs):
        data = self.get_json(
            self.userinfo_url(),
            params={
                'name': True,
                'family_name': True,
                'email': True,
                'email_verified': True
            },
            headers={
                'Authorization': 'Bearer {0}'.format(access_token)
            }
        )
        return data

    def request_access_token(self, *args, **kwargs):
        # This method is overridden, because Turkcell Fast Login does not use id_token
        # and does not provide keys to decode JWT.
        response = self.get_json(*args, **kwargs)
        return response
